import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Switch, Redirect, Route } from 'react-router-dom';
import Dashboard from './components/Dashboard/Dashboard';
import SignIn from './components/SignIn/SignIn';
import SignUp from './components/SignUp/SignUp';
import PrivateRoute from './helper/PrivateRoute';
import PublicRoute from './helper/PublicRoute';
import { isLogin } from './utils';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route
            exact
            path="/"
            render={() => {
              return (
                isLogin ?
                  <Redirect to="/signin" /> :
                  <Redirect to="/dashboard" />
              )
            }}
          />
          <PublicRoute restricted={true} component={SignIn} path="/signin" exact />
          <PublicRoute restricted={true} component={SignUp} path="/signup" exact />
          <PrivateRoute component={Dashboard} path="/dashboard" exact />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;