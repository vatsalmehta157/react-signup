import React from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import TextField from '@material-ui/core/TextField';
import styles from './SignUp.module.scss';
import Button from '@material-ui/core/Button';

const ValidatedSignUpForm = (props) => {
    let { handelSubmit } = props;
    return (
        <Formik
            initialValues={{ email: "", password: "" }}
            onSubmit={(values, { setSubmitting }) => {
                handelSubmit(values);
                setSubmitting(false);
            }}
            //********Using Yum for validation********/
            validationSchema={Yup.object().shape({
                name: Yup.string().required("Required"),
                email: Yup.string()
                    .email()
                    .required("Required"),
                password: Yup.string()
                    .required("No password provided.")
                    .min(8, "Password is too short - should be 8 chars minimum.")
                    .matches(/(?=.*[0-9])/, "Password must contain a number."),
                changepassword: Yup.string().when("password", {
                    is: val => (val && val.length > 0 ? true : false),
                    then: Yup.string().oneOf(
                        [Yup.ref("password")],
                        "Both password need to be the same"
                    )
                })
            })}
        >
            {props => {
                const {
                    values,
                    touched,
                    errors,
                    isSubmitting,
                    handleChange,
                    handleBlur,
                    handleSubmit
                } = props;
                return (
                    <form onSubmit={handleSubmit} className={styles.loginForm}>

                        <TextField name="name"
                            type="text" label="Name" variant="outlined" onChange={handleChange}
                            onBlur={handleBlur} />
                        {errors.name && touched.name && (
                            <div className={styles.errorMSG}>{errors.name}</div>
                        )}

                        <TextField name="email"
                            type="text" label="Email" variant="outlined" onChange={handleChange}
                            onBlur={handleBlur} />
                        {errors.email && touched.email && (
                            <div className={styles.errorMSG}>{errors.email}</div>
                        )}

                        <TextField name="password"
                            type="password" label="Password" variant="outlined" onChange={handleChange}
                            onBlur={handleBlur} />
                        {errors.password && touched.password && (
                            <div className={styles.errorMSG}>{errors.password}</div>
                        )}

                        <TextField name="changepassword"
                            type="password" label="Confirm Password" variant="outlined" onChange={handleChange}
                            onBlur={handleBlur} />
                        {errors.changepassword && touched.changepassword && (
                            <div className={styles.errorMSG}>{errors.changepassword}</div>
                        )}

                        <Button type="submit" variant="contained" color="primary" disabled={isSubmitting}>
                            Sign up
                        </Button>
                    </form>
                );
            }}
        </Formik>
    )
};

export default ValidatedSignUpForm;
