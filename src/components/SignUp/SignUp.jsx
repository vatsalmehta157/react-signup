import React, { useState } from 'react'
import ValidatedSignUpForm from './ValidatedSignUpForm';
import styles from './SignUp.module.scss';
import Card from '@material-ui/core/Card';
import { Typography } from '@material-ui/core';

const SignUp = (props) => {
    let [error, setError] = useState();

    const validateUserExistsOrNot = async (users, email) => {

        let isAvailable = false;
        if (users.length === 0)
            return isAvailable;

        users.forEach(item => {
            if (item.email === email)
                isAvailable = true
        });

        return isAvailable;
    }

    const handelSubmit = async (value) => {
        let userRecords = JSON.parse(localStorage.getItem('userRecords')) || [];

        if (await validateUserExistsOrNot(userRecords, value.email)) {
            console.log("Error: User already exist");
            setError("User already exist!");
            return false;
        }

        userRecords.push(value);
        localStorage.setItem('userRecords', JSON.stringify(userRecords));
        localStorage.setItem('isLoggedIn', true);
        props.history.push('/signin')
    }

    return (
        <div className={styles.mainDiv}>
            <Card className={styles.loginBox}>
                <Typography className={styles.signinTitle}>Sign up</Typography>
                <ValidatedSignUpForm handelSubmit={handelSubmit} />
                <div className={styles.errorMSG}>{error}</div>
            </Card>
        </div>
    )
};

export default SignUp;