import React from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import TextField from '@material-ui/core/TextField';
import styles from './SignIn.module.scss';
import Button from '@material-ui/core/Button';

const ValidatedLoginForm = (props) => {
    let { handelSubmit } = props;
    return (
        <Formik
            initialValues={{ email: "", password: "" }}
            onSubmit={(values, { setSubmitting }) => {
                handelSubmit(values);
                setSubmitting(false);
            }}
            //********Using Yum for validation********/
            validationSchema={Yup.object().shape({
                email: Yup.string()
                    .email()
                    .required("Required"),
                password: Yup.string()
                    .required("No password provided.")
                    .min(8, "Password is too short - should be 8 chars minimum.")
                    .matches(/(?=.*[0-9])/, "Password must contain a number.")
            })}
        >
            {props => {
                const {
                    values,
                    touched,
                    errors,
                    isSubmitting,
                    handleChange,
                    handleBlur,
                    handleSubmit
                } = props;
                return (
                    <form onSubmit={handleSubmit} className={styles.loginForm}>
                        <TextField name="email"
                            type="text" label="Email" variant="outlined" onChange={handleChange}
                            onBlur={handleBlur} />
                        {errors.email && touched.email && (
                            <div className={styles.errorMSG}>{errors.email}</div>
                        )}

                        <TextField name="password"
                            type="password" label="Password" variant="outlined" onChange={handleChange}
                            onBlur={handleBlur} />
                        {errors.password && touched.password && (
                            <div className={styles.errorMSG}>{errors.password}</div>
                        )}

                        <Button type="submit" variant="contained" color="primary" disabled={isSubmitting}>
                            Login
                        </Button>
                    </form>
                );
            }}
        </Formik>
    )
};

export default ValidatedLoginForm;
