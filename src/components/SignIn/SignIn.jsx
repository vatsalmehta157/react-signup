import React, { useState } from 'react'
import ValidatedLoginForm from './ValidatedLoginForm';
import styles from './SignIn.module.scss';
import Card from '@material-ui/core/Card';
import { Typography } from '@material-ui/core';
import { Link } from "react-router-dom";

const SignIn = (props) => {
    let [error, setError] = useState();

    const checkUserCredentials = async (users, payload) => {

        let allowAccess = false
        if (users.length === 0)
            return allowAccess;

        users.forEach(item => {
            if (item.email === payload.email && item.password === payload.password)
                allowAccess = true
        });

        return allowAccess;
    }

    const handelSubmit = async (value) => {
        let userRecords = JSON.parse(localStorage.getItem('userRecords')) || [];
        let allowToAccess = await checkUserCredentials(userRecords, value)
        if (!allowToAccess) {
            console.log("Error: User not found");
            setError("Something went wrong!");
            return false;
        }

        localStorage.setItem('isLoggedIn', true);
        props.history.push('/dashboard')
    }

    return (
        <div className={styles.mainDiv}>
            <Card className={styles.loginBox}>
                <Typography className={styles.signinTitle}>Sign in</Typography>
                <ValidatedLoginForm handelSubmit={handelSubmit} />
                <div className={styles.errorMSG}>{error}</div>
                <div className={styles.signupTotal}>
                    <Link to="/signup">Signup</Link>
                </div>
            </Card>
        </div>
    )
};

export default SignIn;