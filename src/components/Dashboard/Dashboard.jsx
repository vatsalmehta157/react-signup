import React, { useEffect, useState } from 'react';
import BasicTable from './BasicTable';
import TextField from '@material-ui/core/TextField';
import styles from './Dashboard.module.scss';
import { logout } from '../../utils';
import Button from '@material-ui/core/Button';

const Dashboard = (props) => {
    let [filterdata, setFilterData] = useState([]);
    let [searchQuery, setSearchQuery] = useState('');

    function createData(name, calories, fat, carbs, protein) {
        return { name, calories, fat, carbs, protein };
    }

    let [data, setData] = useState([
        createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
        createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
        createData('Eclair', 262, 16.0, 24, 6.0),
        createData('Cupcake', 305, 3.7, 67, 4.3),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
    ]);

    useEffect(() => {
        setFilterData(data)
    }, [data])

    const filterOnSearch = keyword => {
        console.log("keyword", keyword);

        if (!keyword || keyword === " " || keyword === "")
            setFilterData([...data]);
        else {
            let itemsToDisplay = [];
            itemsToDisplay = data.filter(
                item => item["name"].toLowerCase().includes(keyword.toLowerCase())
            );
            setFilterData([...itemsToDisplay]);
        }
    };

    useEffect(() => {
        filterOnSearch(searchQuery);
    }, [searchQuery])

    const deleteItem = (row) => {
        setData(data.filter(item => item.name.toLowerCase() !== row.name.toLowerCase()))
    }

    return (
        <div className={styles.mainDiv}>
            <TextField
                className={styles.searchBox}
                id="standard-helperText"
                label="Search Dessert"
                variant="outlined"
                onChange={(e) => setSearchQuery(e.target.value)}
            />
            {filterdata.length > 0 &&
                <BasicTable rows={filterdata} deleteItem={deleteItem} />
            }

            <Button variant="contained" color="primary" onClick={() => {
                logout()
                props.history.push('/signin')
            }} className={styles.logout}>
                Logout
            </Button>
        </div>
    );
};

export default Dashboard;